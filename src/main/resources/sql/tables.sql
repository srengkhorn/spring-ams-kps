CREATE TABLE tbl_categories (
	id INT PRIMARY KEY auto_increment,
	name VARCHAR NOT NULL
);

CREATE TABLE tbl_articles (
	id INT PRIMARY KEY auto_increment,
	title VARCHAR NOT NULL,
	description VARCHAR NOT NULL,
	thumbnail VARCHAR NOT NULL,
	author VARCHAR NOT NULL,
	created_date VARCHAR NOT NULL,
	category_id INT REFERENCES tbl_categories(id)
);