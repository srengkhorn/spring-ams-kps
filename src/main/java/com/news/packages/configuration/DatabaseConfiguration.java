package com.news.packages.configuration;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
public class DatabaseConfiguration {
	
	@Bean
	@Profile("kpsDB")
	public DataSource dataSourceKPS() {
		DriverManagerDataSource db = new DriverManagerDataSource();
		db.setDriverClassName("org.postgresql.Driver");
		db.setUrl("jdbc:postgresql://localhost:5432/stockDB");
		db.setUsername("postgres");
		db.setPassword("12345678");
		return db;
	}
	
	// In Memory Database using H2
	@Bean
	@Profile("memoryDB")
	public DataSource memoryDataSource() {
		EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
		builder.setType(EmbeddedDatabaseType.H2);
		builder.addScripts("sql/tables.sql", "sql/data.sql");
		return builder.build();
	}
}
