package com.news.packages.services;

import java.util.List;

import com.news.packages.models.Category;

public interface CategoryService {
	List<Category> findAll();
	Category findOne(int id);
	boolean remove(int id);
	boolean save(Category category);
	boolean update(Category category);
}
