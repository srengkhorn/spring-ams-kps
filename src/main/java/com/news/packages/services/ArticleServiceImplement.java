package com.news.packages.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.news.packages.models.Article;
import com.news.packages.models.ArticleFilter;
import com.news.packages.repository.ArticleRepository;

@Service
public class ArticleServiceImplement implements ArticleService {
	
	private ArticleRepository articleRepository;
	
	@Autowired
	public ArticleServiceImplement(ArticleRepository articleRepository) {
		this.articleRepository = articleRepository;
	}

	@Override
	public List<Article> findAll() {
		return articleRepository.findAll();
	}

	@Override
	public Article findOne(int id) {
		return articleRepository.findOne(id);
	}

	@Override
	public boolean remove(int id) {
		return articleRepository.remove(id);
	}

	@Override
	public boolean save(Article article) {
		return articleRepository.save(article);
	}

	@Override
	public boolean update(Article article) {
		return articleRepository.update(article);
	}

	@Override
	public List<Article> findAllFilter(ArticleFilter filter) {
		return articleRepository.findAllFilter(filter);
	}
	
}
