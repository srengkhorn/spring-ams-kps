package com.news.packages.services;

import java.util.List;

import com.news.packages.models.Article;
import com.news.packages.models.ArticleFilter;

public interface ArticleService {
	List<Article> findAll();
	Article findOne(int id);
	boolean remove(int id);
	boolean save(Article article);
	boolean update(Article article);
	
	List<Article> findAllFilter(ArticleFilter filter);
}
