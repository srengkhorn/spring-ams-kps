package com.news.packages.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.news.packages.models.Category;
import com.news.packages.repository.CategoryRepository;

@Service
public class CategoryServiceImplement implements CategoryService {
	
	private CategoryRepository categoryRepository;
	
	@Autowired
	public CategoryServiceImplement(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}

	@Override
	public List<Category> findAll() {
		return categoryRepository.findAll();
	}

	@Override
	public Category findOne(int id) {
		return categoryRepository.findOne(id);
	}

	@Override
	public boolean remove(int id) {
		return categoryRepository.remove(id);
	}

	@Override
	public boolean save(Category category) {
		return categoryRepository.save(category);
	}

	@Override
	public boolean update(Category category) {
		return categoryRepository.update(category);
	}
	
}
