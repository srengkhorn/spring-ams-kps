package com.news.packages.services.upload;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.context.annotation.Description;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.news.packages.models.Article;

@Service
public class UploadService {
	
	@Description("Single upload file")
	public String upload(MultipartFile file) throws IOException {
		
		String uploadPath = "/opt/images/";
		File path = new File(uploadPath);
		if(!path.exists()) {
			path.mkdirs();
		}
		
		// generate random file name with UUID
		String fileName = file.getOriginalFilename();
		fileName = UUID.randomUUID() + "." + fileName.substring(fileName.lastIndexOf(".") + 1);
		System.out.println(fileName);
		
		// upload file to project path
		Files.copy(file.getInputStream(), Paths.get(uploadPath, fileName));
		
		return "/images/" + fileName;
	}
	

	//	<input type="file" name="file" multiple="multiple"/>
	public List<String> uploads(List<MultipartFile> files) throws IOException {
		
		List<String> fileGroup = new ArrayList<String>();
		String uploadPath = "/opt/images/";
		File path = new File(uploadPath);
		
		if(!path.exists()) {
			path.mkdirs();
		}
		
		for(MultipartFile file: files) {
			
			String fileName = file.getOriginalFilename();
			// generate random file name with UUID
			fileName = UUID.randomUUID() + "." + fileName.substring(fileName.lastIndexOf(".") + 1);
			fileGroup.add(fileName); 
			// upload file to project path
			Files.copy(file.getInputStream(), Paths.get(uploadPath, fileName));
		}
		
		return fileGroup;
	}
}
