package com.news.packages.services.upload;

import org.springframework.context.annotation.Description;
import org.springframework.web.multipart.MultipartFile;

public interface Upload {
	
	@Description("Single upload file")
	public String upload(MultipartFile file);
}
