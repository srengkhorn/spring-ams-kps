package com.news.packages.models;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class Article {
	
	private int id;
	
	@Size(min=5, max=100)
	@NotEmpty
	private String title;
	
	@NotEmpty
	private String description;
	private String thumbnail;
	private String author;
	private List<String> images;
	private Category category;
	private String createdDate;
	
	public Article() {}

	public Article(int id, @Size(min = 5, max = 100) @NotEmpty String title, @NotEmpty String description,
			String thumbnail, String author, List<String> images, Category category, String createdDate) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.thumbnail = thumbnail;
		this.author = author;
		this.images = images;
		this.category = category;
		this.createdDate = createdDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + ", description=" + description + ", thumbnail=" + thumbnail
				+ ", author=" + author + ", images=" + images + ", category=" + category + ", createdDate="
				+ createdDate + "]";
	}

	
	

	
	
}
