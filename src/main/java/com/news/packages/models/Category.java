package com.news.packages.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class Category {
	
	private int id;

	@NotEmpty
	private String title;
	
	public Category() {}

	public Category(int id, @Size(min = 5, max = 100) @NotEmpty String title) {
		super();
		this.id = id;
		this.title = title;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "Category [id=" + id + ", title=" + title + "]";
	}

	
	
	
}
