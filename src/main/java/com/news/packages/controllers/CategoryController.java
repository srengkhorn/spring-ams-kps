package com.news.packages.controllers;


import java.io.IOException;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.news.packages.models.Article;
import com.news.packages.models.Category;
import com.news.packages.services.CategoryService;

@Controller
public class CategoryController {
	
	private CategoryService categoryService;

	@Autowired
	public CategoryController(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	
	@GetMapping("/categories")
	public String index(ModelMap category) {
		category.addAttribute("categories", categoryService.findAll());
		return "admin/category/index";
	}
	
	@GetMapping("/category/add")
	public String add(ModelMap model) {
		model.addAttribute("addForm", true);
		model.addAttribute("categories", categoryService.findAll());
		model.addAttribute("category", new Category());
		return "admin/category/add";
	}
	
	@PostMapping("/category/add")
	public String saveData(@Valid Category category, BindingResult bindingResult) throws IOException {
		if (bindingResult.hasErrors()) {
            return "admin/category/add";
        }
		
		if(categoryService.save(category)) {
			System.out.println("Saved successfully!");
		}
		return "redirect:/categories";
	}
	
	@RequestMapping("/category/edit/{id}")
	public String edit(@PathVariable("id") int id, ModelMap model) {
		model.addAttribute("addForm", false);
		model.addAttribute("category", categoryService.findOne(id));
		return "admin/category/add";
	}
	
	@PostMapping("/category/update")
	public String updateData(@Valid Category category, BindingResult result) throws IOException {
		if (result.hasErrors()) {
            return "redirect:/categories";
        }
		
		if(categoryService.update(category)) {
			System.out.println("Updated Successfully!");
		}
		return "redirect:/categories";
	}
	
	@GetMapping("/category/remove/{id}")
	public String remove(@PathVariable("id") int id, ModelMap model) {	
		if(categoryService.remove(id)) {
			System.out.println("Deleted Successfully!");
		}
		return "redirect:/categories";
	}
}
