package com.news.packages.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.news.packages.models.Article;
import com.news.packages.models.ArticleFilter;
import com.news.packages.services.ArticleService;
import com.news.packages.services.CategoryService;
import com.news.packages.services.upload.UploadService;

@Controller
public class ArticleController {
	
	private ArticleService articleService;
	private CategoryService categoryService;
	
	@Autowired
	public ArticleController(ArticleService articleService, CategoryService categoryService) {
		this.articleService = articleService;
		this.categoryService = categoryService;
	}
	
	@RequestMapping(value = {"/", "/dashboard"}, method=RequestMethod.GET)
	public String articles(ArticleFilter filter, ModelMap model) {
		List<Article> articles = articleService.findAllFilter(filter);
		model.addAttribute("articles", articles);
		model.addAttribute("filter", filter);
		model.addAttribute("categories", categoryService.findAll());
		return "admin/article/index";
	}
	
	@RequestMapping(value = "/article/view/{id}", method=RequestMethod.GET)
	public String articleOne(@PathVariable("id") int id, ModelMap model) {
		Article article = articleService.findOne(id);
		model.addAttribute("article", article);
		return "pages/article";
	}
	
	@RequestMapping(value = "/article/add", method=RequestMethod.GET)
	public String add(ModelMap model) {
		model.addAttribute("addForm", true);
		model.addAttribute("categories", categoryService.findAll());
		model.addAttribute("article", new Article());
		return "admin/article/add";
	}
	
	@Autowired
	private UploadService uploadService;
	
	@PostMapping("/article/add")
	public String saveData(@RequestParam("file") MultipartFile file, @Valid Article article, BindingResult bindingResult) throws IOException {
		if (bindingResult.hasErrors()) {
            return "admin/article/add";
        }
		article.setCategory(categoryService.findOne(article.getCategory().getId()));
		String thumbnail = uploadService.upload(file);
		article.setThumbnail(thumbnail);
		article.setAuthor("Admin");
		article.setCreatedDate(new Date().toString());
		
		if(articleService.save(article)) {
			System.out.println("Saved successfully!");
		}
		return "redirect:/dashboard";
	}
	
	@RequestMapping(value = "/article/edit/{id}", method=RequestMethod.GET)
	public String edit(@PathVariable("id") int id, ModelMap model) {
		model.addAttribute("addForm", false);
		model.addAttribute("categories", categoryService.findAll());
		model.addAttribute("article", articleService.findOne(id));
		return "admin/article/add";
	}
	
	@PostMapping("/article/update")
	public String updateData(@RequestParam("file") MultipartFile file, @Valid Article article, BindingResult result) throws IOException {
		System.out.println(article);
		if (result.hasErrors()) {
            return "redirect:/dashboard";
        }
		
		article.setCategory(categoryService.findOne(article.getCategory().getId()));
		String thumbnail = uploadService.upload(file);
		article.setThumbnail(thumbnail);
		article.setAuthor("Admin");
		article.setCreatedDate(new Date().toString());
		
		if(articleService.update(article)) {
			System.out.println("Updated Successfully!");
		}
		return "redirect:/dashboard";
	}
	
	@GetMapping("/article/remove/{id}")
	public String remove(@PathVariable("id") int id, ModelMap model) {	
		if(articleService.remove(id)) {
			System.out.println("Deleted Successfully!");
		}
		return "redirect:/dashboard";
	}
	
}
