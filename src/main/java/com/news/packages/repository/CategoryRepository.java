package com.news.packages.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.news.packages.models.Category;

@Repository
public interface CategoryRepository {
	
	@Select("SELECT * FROM tbl_categories ORDER BY id DESC")
	@Results({
		@Result(property="title", column="name")
	})
	public List<Category> findAll();
	
	@Select("SELECT * FROM tbl_categories WHERE id=#{id}")
	@Results({
		@Result(property="title", column="name")
	})
	public Category findOne(int id);
	
	@Delete("DELETE FROM tbl_categories WHERE id=#{id}")
	public boolean remove(int id);
	
	@Insert("INSERT INTO tbl_categories(name) VALUES(#{title})")
	public boolean save(Category category);
	
	@Update("UPDATE tbl_categories SET name=#{title} WHERE id=#{id}")
	public boolean update(Category category);
}
