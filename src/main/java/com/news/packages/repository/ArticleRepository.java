package com.news.packages.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.news.packages.models.Article;
import com.news.packages.models.ArticleFilter;
import com.news.packages.repository.provider.ArticleProvider;


@Repository
public interface ArticleRepository {
	
	@Select("SELECT a.id, a.title, a.description, a.author, a.thumbnail, a.created_date, a.category_id, c.name as category_name "
			+ "FROM tbl_articles a "
			+ "JOIN tbl_categories c ON a.category_id = c.id "
			+ "ORDER BY a.id DESC")
	@Results({
		@Result(property="createdDate", column="created_date"),
		@Result(property="category.id", column="category_id"),
		@Result(property="category.title", column="category_name")
	})
	public List<Article> findAll();
	
	@Select("SELECT a.id, a.title, a.description, a.author, a.thumbnail, a.created_date, a.category_id, c.name as category_name "
			+ "FROM tbl_articles a "
			+ "JOIN tbl_categories c ON a.category_id = c.id "
			+ "WHERE a.id = #{id}")
	@Results({
		@Result(property="id", column="id"),
		@Result(property="title", column="title"),
		@Result(property="description", column="description"),
		@Result(property="author", column="author"),
		@Result(property="thumbnail", column="thumbnail"),
		@Result(property="createdDate", column="created_date"),
		@Result(property="category.id", column="category_id"),
		@Result(property="category.title", column="category_name")
	})
	public Article findOne(int id);
	
	@Insert("INSERT INTO tbl_articles(title, description, author, thumbnail, created_date, category_id) VALUES(#{title}, #{description}, #{author}, #{thumbnail}, #{createdDate}, #{category.id})")
	public boolean save(Article article);
	
	@Update("UPDATE tbl_articles SET title=#{title}, description=#{description}, author=#{author}, category_id=#{category.id} WHERE id=#{id}")
	public boolean update(Article article);

	@Delete("DELETE FROM tbl_articles WHERE id=#{id}")
	public boolean remove(int id);
	
	@SelectProvider(method = "findAllFilter", type=ArticleProvider.class)
	@Results({
		@Result(property="createdDate", column="created_date"),
		@Result(property="category.id", column="category_id"),
		@Result(property="category.title", column="category_name")
	})
	public List<Article> findAllFilter(ArticleFilter filter);
	
}
